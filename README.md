#### 프로젝트 초기화
저장소 복제 후 최초 한번 프로젝트를 초기화 한다.
```bash
npm install express
```

# jwt 인증 처리
> 로그인 인증 완료 후 인가된 사용자 정보는 전통적으로 세션에 담아 필요시 프로세스에서 호출하여 사용 하였다. 그러나 세션은 하나의 서버에 종속되므로 MSA환경과 API를 통한 웹리소스 자원을 접근하는 최근 트렌드에는 적합하지 않다. 이를 해결하기 위해 나온 기술이 jwt이다. 인증 완료된 사용자 정보를 토큰에 저장하고 각 서버에서는 토큰의 유효성을 체크하여 멀티서버 환경에서 세션 없이도 인증과 사용자의 정보를 사용할 수 있게 된다.

- jwt 토큰 검증기능 생성
- jwt 토큰 검증기능 확인


### jwt 토큰 검증기능 생성
jwt 토큰을 생성하기 위해 jsonwebtoken 모듈을 추가한다.
```bash
npm install jsonwebtoken --save
```


#### 1) 토큰 유효성 검사 모듈 생성
로그인 처리시 생성된 토큰 정보가 유요한지 확인하기 위애서 필요한 모듈을 만든다.
먼저 모듈을 위한  js 파일을 다음과 같이 생성한다.
```
~/auth/verify-token.js
```  

모듈에 사용되는 라이브러리 jsonwebtoken 을 추가한다. 
```javascript   
const jwt = require('jsonwebtoken');
```
이제 필요한 체크 로직 함수를 만든다.
req, res, next 를 인자값으로 한다. 헤더에서 토큰을 읽어 존재 하지 않으면 401 에러를 반환한다. 토큰이 존재하면 유효한지 검사하기 위해 jwt.veify()를 이용하면 된다. 이때 토큰을 만들때 사용한 비밀키를 동일하게 사용해야 한다.   
검증이 완료되면 사용자 정보 객체를 req.user에 저장하고 next()함수룰 통해 다음 프로세스를 진행한다. 이후 사용자 정보가 필요하면 req.user에서 사용자 개인 식별번호(여기서는 _id로 저장했음)를 이용하여 디비에서 검색하여 사용하면 된다.
```javascript   
module.exports =   function (req, res, next){
    const token = req.header('auth-token');
    if(!token) return res.status(401).send('Access Denied');

    try {
        const verified = jwt.verify(token, process.env.TOKEN_SEC_KEY);
        req.user = verified;
        next();  // 처리가 완료되면 다음 메소드로 진행 함
    } catch (error) {
        res.status(400).send('Invalid Token');
    }

};
```

#### 2) 인증이 필요한 신규 API 생성
인증이 필요한 신규 API 를 생성하도록 한다.

##### (1) 신규 API  생성
인증이 완료된 사용자만 접근할 수 있는 신규 API 파일을 다음과 같이 route 폴더에 추가한다.
```
~/route/bbs.js
```  
api 추가를 위해 express 의 Router 가 필요하다. 그리고 여기에 적용할 토큰 유효성 검증을 위해 위에서 만든 모듈을 추가한다,
```javascript   
const router = require("express").Router(); // api를 router를 이용하여 작성한다. 
const verify = require('../auth/verify-token'); // token  검사 

module.exports = router;
```
##### (3) 토큰 유효성 검사 모듈 적용
GET 메소드가 적용된 간단한 API를 추가해 보자. 이전과 다른 점은 웹 주소 바로 다음에 검증 모듈을 추가한다. 아래 주소로 웹 요청이 들어오면 아래 코드 블록이 실행되기 이전에 먼저 웹 토큰 검증(verify)이 수행된다. 검증이 성공적으로 처리가 완료되면 아래 코드 블럭이 실행된다.   
 만약 잘못된 토큰값으로 아래 자원을 호출할 경우 검증 로직에서 더이상 진행하지 않고 401 메시지와 함께 에러 메시지를 반환하며 요청이 종료된다. 
```javascript   
router.get('/',verify, (req, res) => {
    res.send('good ~~');
});
```

#### 3) 외부에 API 노출
신규로 작성한 API를 외부로 노출시켜야 클라이언트에서 접속이 가능하다. 서버 메인실행 파일을 수정해야 한다. 
신규로 적성한 bbs.js 라우터를 app.js에 추가한다.   
```javascript   
const authBbs = require('./routes/bbs'); // bbs api
```

서버에서 다음 주소를 prefix로 하여 api를 사용하겠다고 정의한다.
```javascript   
app.use("/api/bbs", authBbs);  // bbs api
```   





### jwt 토큰 검증기능 확인
jwt 유효성 검증 기능이 잘 동작 하는지 확인해 보자

#### 1) jwt 토큰 생성(로그인)
- 유용한 사용자로 로그인 요청시 아래와 같이 토큰이 생성된다.
 <center><img src="./images/jwt_01.png"  width="80%"/></center>


- 헤더정보를 확인하면 auth-token으로 동일한 토큰 정보가 추가된 것을 확인할 수 있다.
<center><img src="./images/jwt_02.png"  width="80%"/></center>


- 이정보를 복사해서 jwt.io 사이트 encoded 툴에 붙여넣기를 하여 디코딩 하면 어떠한 값이 토큰 정보로 되어 있는지 확인할 수 있다.
<center><img src="./images/jwt_03.png"  width="80%"/></center>



- jwt의 값과 디비값이 동일한지 디비에서 확인해 본다. 동일한 사용자 아이디임을 확인할 수 있다.
<center><img src="./images/jwt_04.png"  width="80%"/></center>


#### 2) 토큰 없이 자원 호출

- 토큰 정보 없이 자원 호출시 접근제한 메시지 리턴됨
<center><img src="./images/jwt_07.png"  width="80%"/></center>


#### 3) 생성된 토큰정보로 자원 호출


- 헤더에 auth-token 키를 추가하고 복사한 토큰을 값으로 넣는다.
<center><img src="./images/jwt_05.png"  width="80%"/></center>




- 토큰 검증을 통과하고 해당 메소드의 처리 결과가 반환된다.
<center><img src="./images/jwt_06.png"  width="80%"/></center>

