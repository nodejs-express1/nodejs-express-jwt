// const Joi = require('joi') // validate
const express = require('express');
const app = express();

// import module
const mongoose = require('mongoose');
const dotenv = require("dotenv"); // environment setup

// import route
const authRouter = require("./routes/auth"); // add a auth
const authBbs = require('./routes/bbs'); // bbs api

 

// Middlewares
app.use(express.json()); // body 를  json으로 변경 

// Router middlewares
app.use("/api/user", authRouter);  // prefix, /api/user/***  <- router에 정의된 resource 주소를 subfix로 함 
app.use("/api/bbs", authBbs);  // bbs api


// 초기 환경 세팅 
dotenv.config();

// DB Connecting
mongoose.connect(
    process.env.MONGODB_CON, 
    {
        useNewUrlParser: true ,
        useUnifiedTopology: true 
    },
    () => {
        console.log("DB connecting ...");
    }
);



// port
const port = process.env.PORT || 3000;
app.listen(port, () =>  console.log(`Server started on  port ${port}`));